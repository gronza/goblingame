﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flames : MonoBehaviour
{
    public List<Transform> spots = new List<Transform>();
    public GameObject flame;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnFlames()
    {
        foreach (Transform t in spots)
        {
            Instantiate<GameObject>(flame,
                t.position,
                t.rotation,
                t);
        }
    }

    public void DestroyFlames()
    {
        foreach (Transform t in spots)
        {
            var psmain = t.GetChild(0).GetComponent<ParticleSystem>().main;
            psmain.loop = false;
            psmain = t.GetChild(0).GetChild(0).GetComponent<ParticleSystem>().main;
            psmain.loop = false;
            psmain = t.GetChild(0).GetChild(1).GetComponent<ParticleSystem>().main;
            psmain.loop = false;

            t.GetChild(0).GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
