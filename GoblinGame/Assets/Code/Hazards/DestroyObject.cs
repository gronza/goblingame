﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public float destroyTimer = 5f;

    private ItemSpawner IS;
    public Transform mySpawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        IS = GameObject.Find("Item Spawn").GetComponent<ItemSpawner>();
        //Destroy(gameObject, destroyTimer);
        StartCoroutine(DestroyMe());
    }

    IEnumerator DestroyMe()
    {
        yield return new WaitForSeconds(destroyTimer);

        IS.emptySpot.Add(mySpawnPoint.transform);
        Destroy(gameObject);
    }
  
}
