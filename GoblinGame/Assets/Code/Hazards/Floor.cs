﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public Quaternion origRot;

    // Start is called before the first frame update
    void Start()
    {
        origRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopShaking()
    {
        GetComponent<Animator>().SetBool("Shaking", false);
        transform.rotation = origRot;
    }
}
