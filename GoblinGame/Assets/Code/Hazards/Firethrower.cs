﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firethrower : MonoBehaviour
{

    public bool rising;
    public bool falling;
    public float riseSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rising = true;
        riseSpeed = 1f;
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position.y >= -0.5f)
            {
                rising = false;
        }

        if (rising)
        {
            transform.Translate(Vector3.up * riseSpeed * Time.deltaTime);

        }
        else if(falling)
        {
            transform.Translate(-Vector3.up * riseSpeed * Time.deltaTime * 2);
            
        }
    }

    public void SetFalling()
    {
        falling = true;
    }
}
