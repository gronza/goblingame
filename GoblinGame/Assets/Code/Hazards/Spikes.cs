﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    public bool rising;
    public bool activated;
    public float setHeight;
    public float activatedHeight;
    public float activationTime;
    public float activationTimer;
    public float activeTime;
    public float activeTimer;
    public bool triggered;
    public bool falling;
    public float lifeTime;
    public float lifeTimer;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        rising = true;
        setHeight = -0.5f;
        activatedHeight = 0.5f;
        activationTime = 0.5f;
        activationTimer = activationTime;
        activeTime = 1f;
        activeTimer = activeTime;

        lifeTime = 6f;
        lifeTimer = lifeTime;

        speed = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //on spawn
        if (rising)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
            if(transform.position.y >= setHeight)
            {
                transform.position = new Vector3(transform.position.x, setHeight, transform.position.z);
                rising = false;
            }
        }

        //when stepped on
        if (triggered)
        {
            activationTimer -= Time.deltaTime;
            if(activationTimer <= 0)
            {
                activationTimer = activationTime;
                triggered = false;
                activated = true;
            }
        }

        //once activated
        if (activated)
        {
            activeTimer -= Time.deltaTime;


            if(transform.position.y < activatedHeight)
            {
                transform.Translate(Vector3.up * speed*5 * Time.deltaTime);
            }

            activated = true;

            if(activeTimer <= 0)
            {
                activeTimer = activeTime;
                activated = false;
                falling = true;
            }
        }

        //falls back after activating
        if (falling)
        {
            transform.Translate(-Vector3.up * speed*5 * Time.deltaTime);

            if(transform.position.y <= setHeight)
            {
                falling = false;
            }
        }

        lifeTimer -= Time.deltaTime;
        if(lifeTimer < 0)
        {
            if(!activated && !falling && !triggered)
            {
                transform.Translate(-Vector3.up * speed * Time.deltaTime);
                if(transform.position.y < -3)
                {
                    GameObject.Find("Item Spawn").GetComponent<ItemSpawner>().emptySpot.Add(GetComponent<DestroyObject>().mySpawnPoint);
                    Destroy(this.gameObject);
                }
            }
        }

    }

    private void OnTriggerStay(Collider other)
    {
        //When colliding with player
        if (other.gameObject.CompareTag("Player"))
        {
            if(!triggered && !activated)
            {
                triggered = true;
            }
            if (activated)
            {
                other.GetComponent<PlayerController>().Stun(gameObject);
            }
        }
    }
}
