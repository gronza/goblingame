﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingDebris : MonoBehaviour
{
    public float fallSpeed = 20f;
    public bool stunning;
    //public float spinSpeed = 5f;
    AudioManager audioManager;
    private bool alreadyPlayed = false;

    private void Start()
    {
        stunning = true;
        audioManager = AudioManager.instance;
    }

    //move
    public void Update()
    {
        //transform.Translate(Vector3.down * fallSpeed * Time.deltaTime);
        //transform.Rotate(Vector3.forward * spinSpeed * Time.deltaTime);
    }

    //destroy da object after a while
    void DestroyObjectDelayed()
    {
        Destroy(gameObject, 10);
    }

    //Stun Player on hit, serves them right
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().Stun(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") && !alreadyPlayed)
        {
            audioManager.PlaySound("RockFall1");
            stunning = false;
            alreadyPlayed = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            stunning = false;
        }

        if (collision.gameObject.tag == "Player")
        {
            if (stunning)
            {
                collision.gameObject.GetComponent<PlayerController>().Stun(gameObject);
                audioManager.PlaySound("Ghit4");
            }
        }
    }
}
