﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gas : MonoBehaviour
{
    public float timer;
    public float gasTime;
    public bool gasSpawned;
    public float fallTime;
    public Firethrower fire;
    public GameObject gas;

    public float gasDuration;

    // Start is called before the first frame update
    void Start()
    {
        gasTime = 2;
        gasDuration = 5;
        timer = 0;
        fire = GetComponent<Firethrower>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer >= gasTime && !gasSpawned)
        {
            gasSpawned = true;
            SpawnGas();
        }

        if(timer >= 3)
        {
            fire.SetFalling();
        }

        //stops gas emission
        if (timer >= gasTime + gasDuration - 1 && gasSpawned)
        {
            var psmain = gas.transform.GetChild(1).GetComponent<ParticleSystem>().main;
            psmain.loop = false;
        }

        //destroys the collider
        
        if (timer >= gasTime+gasDuration - 0.1f && gasSpawned)
        {
            Destroy(gas.GetComponent<SphereCollider>());
        }
        
    }

    void SpawnGas()
    {
        var g = Instantiate<GameObject>(gas,
            transform.position,
            transform.rotation);
        g.transform.Translate(Vector3.up, Space.World);

        gas = g;

        Destroy(g, gasDuration * 2);

    }
}
