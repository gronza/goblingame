﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{

    public float delay = 5f;
    public float radius = 5f;
    float countdown;
    bool hasExploded = false;

    public float force = 10;

    public GameObject bombEffect;

    AudioManager audioManager;
    private bool alreadyPlayed;

    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;

        audioManager = AudioManager.instance;
        audioManager.PlaySound("BombFuse");

    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    void Explode()
    {

        Instantiate(bombEffect, transform.position, new Quaternion());
        audioManager.PlaySound("BombExp");

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider nearbyObject in colliders)
        {
            //explosionforce mikäli haluaa tuhota jotain

            /*Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            //if (rb != null)
            //{
            //rb.AddExplosionForce(force, transform.position, radius);
            }*/

            if (nearbyObject.gameObject == transform.parent)
            {
                Physics.IgnoreCollision(GetComponent<BoxCollider>(), transform.parent.GetComponent<CapsuleCollider>());
            }
            else if (nearbyObject.CompareTag("Player"))
            {
                //Destroy(nearbyObject.gameObject);
                //nearbyObject.GetComponent<PlayerController>().stunned = true;
                nearbyObject.GetComponent<PlayerController>().Stun(gameObject);
            }
        }

        GameObject.Find("Item Spawn").GetComponent<ItemSpawner>().emptySpot.Add(GetComponent<DestroyObject>().mySpawnPoint);
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") && !alreadyPlayed)
        {
            audioManager.PlaySound("RockFall4");
            alreadyPlayed = true;
            
        }

        if (collision.gameObject.CompareTag("Floor"))
        {

        }
    }
}
