﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [CreateAssetMenu(fileName = "InputConfig", menuName = "Input Configuration", order = 1)]
    public class InputConfig : ScriptableObject
    {
        public enum ControllerType
        {
            None = 0,
            Keyboard,
            Keyboard2,
            Gamepad1,
            Gamepad2,
            Gamepad3,
            Gamepad4
        }

        public enum Action
        {
            None = 0,
            Horizontal,
            Vertical,
            Fire1,
            Fire2,
            Submit,
            Cancel
        }

        /// <summary>
        /// Määrittää, minkä niminen Unityn input-systeemin akseli liittyy määritettyyn
        /// Input Actioniin
        /// </summary>
        [System.Serializable]
        public class InputMap
        { 
            public Action InputAction;
            public string AxisName;
        }

        [System.Serializable]
        public class InputMapping
        {
            public ControllerType Controller;
            public List< InputMap > InputMaps = new List< InputMap >();
        }

        [SerializeField]
        private List<InputMapping> inputMappings = new List< InputMapping >();

        public float GetAxis( ControllerType controller, Action inputAction )
        {
            foreach ( var inputMapping in inputMappings )
            {
                if ( inputMapping.Controller == controller )
                {
                    foreach ( var map in inputMapping.InputMaps )
                    {
                        if ( map.InputAction == inputAction )
                        {
                            return Input.GetAxis( map.AxisName );
                        }
                    }
                }
            }

            return float.NaN;
        }

        public bool? GetButton( ControllerType controller, Action inputAction )
        {
            string axisName = GetInputAxis( controller, inputAction );
            if ( axisName != null )
            {
                return Input.GetButton( axisName );
            }

            return null;
        }

        public bool? GetButtonDown(ControllerType controller, Action inputAction)
        {
            string axisName = GetInputAxis(controller, inputAction);
            if (axisName != null)
            {
                return Input.GetButtonDown(axisName);
            }

            return null;
        }

        public bool? GetButtonUp(ControllerType controller, Action inputAction)
        {
            string axisName = GetInputAxis(controller, inputAction);
            if (axisName != null)
            {
                return Input.GetButtonUp(axisName);
            }

            return null;
        }

        private InputMapping GetInputMapping( ControllerType controller )
        {
            foreach ( var inputMapping in inputMappings )
            {
                if ( inputMapping.Controller == controller )
                {
                    return inputMapping;
                }
            }

            return null;
        }

        private string GetInputAxis( ControllerType controller, Action inputAction )
        {
            InputMapping mapping = GetInputMapping( controller );
            if ( mapping == null )
            {
                return null;
            }

            foreach ( var map in mapping.InputMaps )
            {
                if ( map.InputAction == inputAction )
                {
                    return map.AxisName;
                }
            }

            return null;
        }
    }
