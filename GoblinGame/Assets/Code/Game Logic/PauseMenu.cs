﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    public bool GameIsPaused = false;
    public GameObject PauseUI;

    //public EventSystem eventSystem;
    //public GameObject pauseFirst;
    

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameIsPaused = !GameIsPaused;
        }

        if (GameIsPaused)
        {
            ActivateMenu();
        }

        else
        {
            DeactivateMenu();
        }
    }


    //Paused
    void ActivateMenu()
    {
        PauseUI.SetActive(true);
        Time.timeScale = 0f;
        AudioListener.pause = true;
        //EventSystem.current.SetSelectedGameObject(null);
        //EventSystem.current.SetSelectedGameObject(pauseFirst);
        //eventSystem.SetSelectedGameObject(null);
        //eventSystem.firstSelectedGameObject = pauseFirst;
    }


    //Continueplaying
    public void DeactivateMenu()
    {   
        Time.timeScale = 1f;
        PauseUI.SetActive(false);
        AudioListener.pause = false;
    }

    public void Resume()
    {
        Debug.Log("Continue");
        Time.timeScale = 1f;
        PauseUI.SetActive(false);
        AudioListener.pause = false;
        GameIsPaused = false;
    }

    //Return to main menu
    public void ExitMenu()
    {
        Debug.Log("Exit");
        Time.timeScale = 1f;
        AudioListener.pause = false;
        GameIsPaused = false;
        SceneManager.LoadScene("MainMenu");
        
    }
}
