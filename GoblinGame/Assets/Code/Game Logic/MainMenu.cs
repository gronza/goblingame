﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void Settings()
    {
        Debug.Log("SettingsPanel");
    }

    public void Credits()
    {
        Debug.Log("CreditsPanel");
    }

    public void ExitGame()
    {
        Debug.Log("Shutdown");
        Application.Quit();
    }
}
