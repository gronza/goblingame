﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldController : MonoBehaviour
{
    public float value;
    public bool pickable;
    public BoxCollider box;
    public CapsuleCollider capsule;

    AudioManager audioManager;
    private bool alreadyPlayed = false;

    // Start is called before the first frame update
    void Start()
    {
        pickable = false;
        box = GetComponent<BoxCollider>();
        capsule = GetComponent<CapsuleCollider>();

        audioManager = AudioManager.instance;

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Player")) {
            Physics.IgnoreCollision(box, g.GetComponent<CapsuleCollider>(), true);
                }
    }

    // Update is called once per frame
    void Update()
    {
        if (pickable)
        {
            capsule.enabled = true;
        }
        else
        {
            capsule.enabled = false;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") && !alreadyPlayed)
        {
            audioManager.PlaySound("Coinfall4");
            alreadyPlayed = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {

        }
    }
}
