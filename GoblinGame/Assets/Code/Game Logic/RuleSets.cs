﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuleSets : MonoBehaviour
{
    public List<RuleSet> ruleSets = new List<RuleSet>();
    public RuleSet currentRules;
    public GoldSpawnerController gsc;
    

    // Start is called before the first frame update
    void Start()
    {
        ruleSets.Add(new NormalRound());
        ruleSets.Add(new BombRound());
        ruleSets.Add(new DoubleRound());
        ruleSets.Add(new GoldRound());
        ruleSets.Add(new RockRound());
        ruleSets.Add(new GasRound());
        ruleSets.Add(new ShieldRound());
        ruleSets.Add(new SpeedRound());
        ruleSets.Add(new SpikeRound());
        ruleSets.Add(new FlameRound());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract class RuleSet
    {
        public Text roundId;
        public GoldSpawnerController gsc;
        public ItemSpawner isp;
        public void Initialize()
        {
            gsc = GameObject.Find("Item Spawn").GetComponent<GoldSpawnerController>();
            roundId = GameObject.Find("Round Id").GetComponent<Text>();
            isp = gsc.GetComponent<ItemSpawner>();
        }
        public abstract void SetRules();
    }

    // FOLLOWING ARE THE DIFFERENT RULESETS

        // Basic round with normal gold and hazard factors
    public class NormalRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(gsc.goldFactor);
            roundId.text = "enter the dungeon";

            // Normal values for hazards to be added later
            isp.SetFactors(isp.hazardFactor);
        }
    }

    public class GoldRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(3);
            roundId.text = "gold rush";

            // Normal values for hazards to be added later
            isp.SetFactors(isp.hazardFactor);

        }
    }

    public class EmptyRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(0);
            roundId.text = "loppukuu";

            // Normal values for hazards to be added later
            isp.SetFactors(15);

        }
    }

    public class BombRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(2f);
            roundId.text = "bombs everywhere";

            // Normal values for hazards to be added later
            isp.SetFactors(30);
            isp.bombFactor = 2f;

        }
    }

    public class DoubleRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(gsc.goldFactor*2);
            roundId.text = "double it all";

            // Normal values for hazards to be added later
            isp.SetFactors(isp.hazardFactor/2);

        }
    }

    public class RockRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(2f);
            roundId.text = "watch the ceiling";

            // Normal values for hazards to be added later
            isp.SetFactors(30);
            isp.rockFactor = 1f;

        }
    }

    public class GasRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(1.5f);
            roundId.text = "feeling gassy";

            // Normal values for hazards to be added later
            isp.SetFactors(30);
            isp.gasFactor = 2f;
            isp.speedBuffFactor = 0.5f;

        }
    }

    public class ShieldRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(1.5f);
            roundId.text = "shields up";

            // Normal values for hazards to be added later
            isp.SetFactors(2f);
            isp.shieldBuffFactor = 1;
            isp.gasFactor = 30f;

        }
    }

    public class SpeedRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(1.5f);
            roundId.text = "go fast";

            // Normal values for hazards to be added later
            isp.SetFactors(isp.hazardFactor);
            isp.speedBuffFactor = 1f;

        }
    }

    public class SpikeRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(1.5f);
            roundId.text = "very sharp";

            // Normal values for hazards to be added later
            isp.SetFactors(30);
            isp.spikeFactor = 3f;

        }
    }

    public class FlameRound : RuleSet
    {
        public override void SetRules()
        {
            Initialize();

            // Sets normal values for valuables
            gsc.SetRound(2f);
            roundId.text = "it's getting hot";

            // Normal values for hazards to be added later
            isp.SetFactors(30);
            isp.flameFactor = 3f;

        }
    }
}
