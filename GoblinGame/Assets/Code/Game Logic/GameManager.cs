﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameManager
{

    private static InputConfig _inputs;

    public static InputConfig Inputs
    {
        get
        {
            if (_inputs == null)
            {
                _inputs = Resources.Load<InputConfig>("InputConfig");
            }

            return _inputs;
        }
    }
}
