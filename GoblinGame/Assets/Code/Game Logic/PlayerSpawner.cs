﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerSpawner : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject chestPrefab;
    public RoundLogic round;

    public int maxPlayers;

    public List<PlayerController> players = new List<PlayerController>();
    public List<GameObject> freeSpawns = new List<GameObject>();
    public List<Text> freeTexts = new List<Text>();
    public List<GameObject> indicators = new List<GameObject>();
    public GameObject indicator1;
    public GameObject indicator2;
    public GameObject indicator3;
    public GameObject indicator4;

    public bool spawning;

    public List<InputConfig.ControllerType> controllers = new List<InputConfig.ControllerType>();
    public InputConfig.ControllerType keyboard;
    public InputConfig.ControllerType keyboard2;
    public InputConfig.ControllerType gamepad1;
    public InputConfig.ControllerType gamepad2;
    public InputConfig.ControllerType gamepad3;
    public InputConfig.ControllerType gamepad4;

    public List<InputConfig.ControllerType> joined = new List<InputConfig.ControllerType>();

    public float playerId;
    public CameraFade camera;

    AudioManager audioManager;

    // Start is called before the first frame update
    public void Start()
    {
        round = GetComponent<RoundLogic>();
        spawning = true;
        maxPlayers = 3;

        keyboard = InputConfig.ControllerType.Keyboard;
        keyboard2 = InputConfig.ControllerType.Keyboard2;
        gamepad1 = InputConfig.ControllerType.Gamepad1;
        gamepad2 = InputConfig.ControllerType.Gamepad2;
        gamepad3 = InputConfig.ControllerType.Gamepad3;
        gamepad4 = InputConfig.ControllerType.Gamepad4;

        controllers.Add(keyboard);
        controllers.Add(keyboard2);
        controllers.Add(gamepad1);
        controllers.Add(gamepad2);
        controllers.Add(gamepad3);
        controllers.Add(gamepad4);

        
        freeSpawns.Add(GameObject.Find("Spawn2"));
        freeSpawns.Add(GameObject.Find("Spawn3"));
        freeSpawns.Add(GameObject.Find("Spawn4"));
        freeSpawns.Add(GameObject.Find("Spawn1"));

        indicators.Add(indicator1);
        indicators.Add(indicator2);
        indicators.Add(indicator3);
        indicators.Add(indicator4);

        camera = GameObject.Find("Camera").GetComponent<CameraFade>();

        audioManager = AudioManager.instance;
    }

    // Update is called once per frame
    void Update()
    {


        //players join the game before it starts
        if (spawning)
        {

            //spawn a player with selected controller
            //and assign all values for it
            for (int i = 0; i < controllers.Count; i++)
            {
                var t = controllers[i];
                    bool? join = GameManager.Inputs.GetButton(controllers[i], InputConfig.Action.Fire1);
                    if (join.HasValue && join == true)
                    {
                             InitiatePlayer(t, freeSpawns[0], joined.Count);
                            //controller and spawn can't be used again for another player
                            joined.Add(t);
                            controllers.RemoveAt(i);
                            freeSpawns.Remove(freeSpawns[0]);

                            //audioManager.PlaySound("Player1Select");
                            //Randomize player select sound
                            //int j = Random.Range(1, 3);
                            switch (i)
                            {
                                case 0:
                                    audioManager.PlaySound("Player1Select");
                                    break;
                                case 1:
                                    audioManager.PlaySound("Player2Select");
                                    break;
                                case 2:
                                    audioManager.PlaySound("Player1Select");
                                    break;
                                case 3:
                                    audioManager.PlaySound("Player2Select");
                                    break;
                            }
                    }
            }

            //inputs from joined players
            for (int i = 0; i < joined.Count; i++)
            {
                var t = joined[i];

                //let players drop out from the spawning screen
                bool? drop = GameManager.Inputs.GetButton(t, InputConfig.Action.Cancel);
                if (drop.HasValue && drop == true)
                {
                    controllers.Add(t);
                    foreach (GameObject g in GameObject.FindGameObjectsWithTag("Player"))
                    {
                        var pc = g.GetComponent<PlayerController>();
                        if (pc.controller == t)
                        {
                            freeSpawns.Insert(0, pc.spawn);
                            indicators.Insert(0, pc.indicator);
                            Destroy(pc.gameObject);
                        }
                    }
                    joined.Remove(t);
                }

                //check if a joined player is starting the game
                //with the START button
                bool? join = GameManager.Inputs.GetButton(t, InputConfig.Action.Submit);
                if (join.HasValue && join == true)
                {
                    spawning = false;

                    StartCoroutine(FadeOut(1));
                    camera.FadeOut(1.5f);

                    StartCoroutine(StartGame());
                }
            }
        }
    }

    //Spawns player in the characterselect scene
    public GameObject InitiatePlayer(InputConfig.ControllerType ct, GameObject s, int i)
    {
        var p = Instantiate<GameObject>(playerPrefab,
                    s.transform.position,
                    s.transform.rotation,
                    transform.GetChild(0).transform);

        p.GetComponent<PlayerController>().spawn = s;
        p.GetComponent<PlayerController>().controller = ct;
        p.GetComponent<PlayerController>().indicator = indicators[0];
        indicators.RemoveAt(0);

        return p;
    }

    //Spawns the player in the actual game scene
    public GameObject SpawnPlayer(int id, InputConfig.ControllerType ct, GameObject s, float t, GameObject g, GameObject i)
    {

        //spawns a treasure chest for the player
        var c = Instantiate<GameObject>(chestPrefab,
            s.transform.position - g.transform.forward,
            g.transform.rotation);
        c.transform.Rotate(new Vector3(0, 90, 0));
        Instantiate(i,
            c.transform.GetChild(0).position,
            Quaternion.Euler(90, 0, 0),
            c.transform.GetChild(0));

        //chest textmesh number
        Text text = freeTexts[0];
        freeTexts.Remove(freeTexts[0]);

           var p = Instantiate<GameObject>(playerPrefab,
                    s.transform.position,
                    g.transform.rotation,
                    transform.GetChild(1));

        var pc = p.GetComponent<PlayerController>();

        //spawns the player and assigns variables for it
        pc.controller = ct;
        pc.score = t;
        pc.chest = c;
        pc.spawn = s;
        pc.scoreText = text;
        pc.name = "Player" + id.ToString();
        pc.round = GameObject.Find("Item Spawn").GetComponent<RoundLogic>();
        pc.indicator = i;
        indicators.RemoveAt(indicators.Count - 1);
        indicators.Insert(0, pc.indicator);

        return p;
    }

    //Called when game scene is loaded, initializes a match
    public IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1.55f);

        GameObject.Find("Item Spawn").GetComponent<RoundLogic>().enabled = true;
        GameObject.Find("Item Spawn").GetComponent<RoundLogic>().Start();

        //Reset spawns
        freeSpawns.Clear();
        freeSpawns.Add(GameObject.Find("Spawn2"));
        freeSpawns.Add(GameObject.Find("Spawn3"));
        freeSpawns.Add(GameObject.Find("Spawn4"));
        freeSpawns.Add(GameObject.Find("Spawn1"));

        freeTexts.Clear();
        freeTexts.Add(GameObject.Find("Text1").GetComponent<Text>());
        freeTexts.Add(GameObject.Find("Text2").GetComponent<Text>());
        freeTexts.Add(GameObject.Find("Text3").GetComponent<Text>());
        freeTexts.Add(GameObject.Find("Text4").GetComponent<Text>());

        indicators.Clear();
        indicators.Add(indicator1);
        indicators.Add(indicator2);
        indicators.Add(indicator3);
        indicators.Add(indicator4);

        //destroy players from selection screen
        //and spawns them in the new scene
        for (int i = 0; i < transform.GetChild(0).childCount; i++)
        {
            Debug.Log("Player spawned");
            
                var p = SpawnPlayer(players.Count + 1,
                        transform.GetChild(0).GetChild(i).GetComponent<PlayerController>().controller,
                        freeSpawns[0],
                        0,
                        freeSpawns[0],
                        transform.GetChild(0).GetChild(i).GetComponent<PlayerController>().indicator);
            

            players.Add(p.GetComponent<PlayerController>());
            freeSpawns.Remove(freeSpawns[0]);

            Destroy(transform.GetChild(0).GetChild(i).gameObject);
        }

        camera = GameObject.Find("Camera").GetComponent<CameraFade>();
        camera.GetComponent<CameraController>().Initialize();
    }

    public void StartReturn()
    {
        StartCoroutine(FadeOut(0));
        camera.FadeOut(1.5f);
        StartCoroutine(ReturnToMenu());
    }

    //Called when selection scene is loaded again
    public IEnumerator ReturnToMenu()
    {
        
        yield return new WaitForSeconds(1.55f);

        players.Clear();

        //Reset spawns
        freeSpawns.Clear();
        freeSpawns.Add(GameObject.Find("Spawn2"));
        freeSpawns.Add(GameObject.Find("Spawn3"));
        freeSpawns.Add(GameObject.Find("Spawn4"));
        freeSpawns.Add(GameObject.Find("Spawn1"));

        indicators.Clear();
        indicators.Add(indicator1);
        indicators.Add(indicator2);
        indicators.Add(indicator3);
        indicators.Add(indicator4);

        camera = GameObject.Find("Camera").GetComponent<CameraFade>();
        audioManager = AudioManager.instance;

        spawning = true;

        //destroy players from game screen
        //and spawns them in the new scene
        for (int i = 0; i < transform.GetChild(1).childCount; i++)
        {
            
            var p = InitiatePlayer(
                    transform.GetChild(1).GetChild(i).GetComponent<PlayerController>().controller,
                    freeSpawns[0],
                    i);
                    
            freeSpawns.Remove(freeSpawns[0]);
            Destroy(transform.GetChild(1).GetChild(i).gameObject);
        }
    }

    public IEnumerator FadeOut(int index)
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(index);
    }
}
