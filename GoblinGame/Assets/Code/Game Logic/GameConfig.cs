﻿using UnityEngine;

    [CreateAssetMenu(fileName = "GameConfig", menuName = "Game Configuration", order = 2)]
    public class GameConfig : ScriptableObject
    {
        // Fieldit asetettu privateksi. Jos ne olisivat publiceja,
        // niiden arvoa olisi mahdollista muuttaa pelin koodista Unityn editorissa.
        // Tällöin arvot eivät pysyisi samoina pelikertojen välillä.

        [SerializeField]
        private int _maxPlayerCount;

        [SerializeField]
        private int _unitLives;

        public int MaxPlayerCount
        {
            get { return _maxPlayerCount; }
        }

        public int UnitLives
        {
            get { return _unitLives; }
        }
    }
