﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoundLogic : MonoBehaviour
{
    public PlayerSpawner spawner;
    public RuleSets rules;
    public GoldSpawnerController gSpawner;
    public ItemSpawner isp;

    public float timer;
    public float roundTime;
    public Text timerText;
    public Text winText;
    public Text titleText;
    public Text roundNumber;

    public bool roundPaused;
    public float pauseTime;
    public float pauseTimer;

    public float roundCount;
    public float currentRound;
    public bool roundSelected;
    public GameObject roundScreen;

    public List<Text> scoreTexts = new List<Text>();
    public List<Text> scoreScreenTexts = new List<Text>();

    public bool returning;
    public bool counting;
    public float countSpeed;

    AudioManager audioManager;
    public bool alreadyPlayed = false;

    // Start is called before the first frame update
    public void Start()
    {
        rules = GetComponent<RuleSets>();
        roundPaused = true;

        //should be 10
        pauseTime = 10;

        pauseTimer = 7;
        winText = GameObject.Find("Winner").GetComponent<Text>();

        //finds all spawners, sets gold spawns in a list and disables spawning
        spawner = GameObject.Find("PlayerSpawner(Clone)").GetComponent<PlayerSpawner>();
        gSpawner = GameObject.Find("Item Spawn").GetComponent<GoldSpawnerController>();
        isp = GetComponent<ItemSpawner>();
        ToggleGold(false);

        roundTime = 30;
        timer = roundTime;

        //should be 3 or 5
        roundCount = 5;

        currentRound = 1;
        roundScreen.SetActive(false);

        returning = false;
        countSpeed = 5;

        audioManager = AudioManager.instance;

    }

    // Update is called once per frame
    void Update()
    {

        //Before gameplay
        if (roundPaused)
        {
            pauseTimer -= Time.deltaTime;
            timerText.text = "";

            //While there are more rounds to come
            if (currentRound <= roundCount)
            {
                //pausetimer starts at 10 so the screen is active for 5

                if (pauseTimer <= 5)
                {
                    //Randomly select a new ruleset for a new round
                    //First round is always the same
                    if (!roundSelected)
                    {
                        if (currentRound == 1)
                        {
                            SelectRound(0);
                        }
                        else
                        {
                            SelectRound(UnityEngine.Random.Range(0, rules.ruleSets.Count));
                        }
                        roundSelected = true;
                    }

                    roundNumber.text = "round " + currentRound + "/" + roundCount;
                    roundScreen.SetActive(false);
                    winText.text = Mathf.Ceil(pauseTimer).ToString();
                }

                if (pauseTimer <= 0)
                {
                    StartRound();
                }
            }

            //After all rounds have been played
            else
            {
                if(pauseTimer < 0 && !returning)
                {
                    returning = true;
                    spawner.StartReturn();
                }
            }
        }

        //During gameplay
        if (!roundPaused)
        {
            var b = Mathf.Ceil(timer);
            timer -= Time.deltaTime;
            var a = Mathf.Ceil(timer);

            if (a > 5)
            {
                timerText.text = a.ToString();
            }

            if(a <= 5)
            {
                timerText.text = "";
                if(a != b)
                {
                    audioManager.PlaySound("Count" + a.ToString());
                }
                
                winText.text = a.ToString();
            }

            if (timer <= 0)
            {
                ResetRound();
            }
        }

        //Slowly counts score between rounds
        if (counting)
        {
            foreach(PlayerController g in spawner.players)
            {
                //slowly count the gold
                if (g.score > 0)
                {
                    if (g.score >= countSpeed)
                    {
                        g.finalScore += countSpeed;
                        g.score -= countSpeed;
                    }
                    else
                    {
                        g.finalScore += g.score;
                        g.score = 0;
                    }
                }
            }

                //Sort players by score and show them in order
                spawner.players.Sort();
                for (int i = 0; i < spawner.players.Count; i++)
                {
                    //Activate standings for players
                    roundScreen.transform.GetChild(1).GetChild(i).gameObject.SetActive(true);

                    //Set player scores and names in order
                    scoreScreenTexts[i].text = spawner.players[i].GetComponent<PlayerController>().name;
                    scoreScreenTexts[i].transform.GetChild(0).GetComponent<Text>().text
                        = spawner.players[i].GetComponent<PlayerController>().finalScore.ToString();
                }


            //Declare winner after final round
            if (currentRound > roundCount)
            {
                titleText.text = "End of Match";
                foreach(PlayerController g in spawner.players)
                {
                    if(g.score > 0)
                    {
                        return;
                    }
                }
                if (!alreadyPlayed)
                {
                    audioManager.PlaySound("GameWin");
                    alreadyPlayed = true;
                }
                titleText.text = spawner.players[0].name + " Wins!";
                
            }
        }
    }

    private void StartRound()
    {
        roundPaused = false;
        ToggleGold(true);
        winText.text = "";
        rules.currentRules.roundId.text = "";
        roundNumber.text = "";
        roundScreen.SetActive(false);
        currentRound += 1;
        counting = false;
        GetComponent<ItemSpawner>().RandomizeStartup();

        //plays a random round start sound
        int i = Random.Range(1, 4);
        switch (i)
        {
            case 1:
                audioManager.PlaySound("Round1");
                break;
            case 2:
                audioManager.PlaySound("Round2");
                break;
            case 3:
                audioManager.PlaySound("Round3");
                break;
        }

        foreach (PlayerController g in spawner.players)
        {
            //Unstuns the players so they can move
            g.stunned = false;
        }
    }

    void ResetRound()
    {
        roundScreen.SetActive(true);
        roundSelected = false;

        //reset timers, stop stuff from spawning during countdown
        timer = roundTime;
        pauseTimer = pauseTime;
        ToggleGold(false);
        roundPaused = true;
        counting = true;
        winText.text = "";
        if (currentRound <= roundCount)
        {
            audioManager.PlaySound("RoundWin");
        }

        //reset player scores
        //add round scores to total scores
        foreach (PlayerController g in spawner.players){
            
            //Tästä tulee (tuli?) roundin alussa liikkumisbugi
            g.Respawn();
        }

        //destroy gold left unpicked from previous round
        var golds = GameObject.FindGameObjectsWithTag("Gold");
        foreach (GameObject g in golds)
        {
            Destroy(g);
        }

        List<GameObject> destroy = new List<GameObject>();
        destroy.AddRange(GameObject.FindGameObjectsWithTag("Hazard"));
        destroy.AddRange(GameObject.FindGameObjectsWithTag("Buff"));
        destroy.AddRange(GameObject.FindGameObjectsWithTag("InvulnerableBuff"));
        //Destroy leftover items too
        foreach (GameObject g in destroy)
        {
            if (g.GetComponent<DestroyObject>())
            {
                isp.emptySpot.Add(g.GetComponent<DestroyObject>().mySpawnPoint);
            }
            if (g.GetComponent<Bomb>()){
                
            }
            Destroy(g);
        }

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("SlowArea"))
        {
            Destroy(g);
        }


        foreach (var g in GetComponents<GoldSpawner>())
        {
            g.spawnTime = g.origSpawnTime;
        }
        //spawner.Start();
    }

    //Toggles gold spawning on or off in every gold spawner
    public void ToggleGold(bool b)
    {
        gSpawner.spawning = b;
        isp.spawning = b;
    }
    
    public void SelectRound(int index)
    {
        rules.currentRules = rules.ruleSets[index];
        rules.currentRules.SetRules();
        rules.ruleSets.Remove(rules.ruleSets[index]);
    }
}
