﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldSpawnerController : MonoBehaviour
{
    public List<GoldSpawner> goldSpawners = new List<GoldSpawner>();

    public float goldFactor;
    public float coinFactor;
    public float crownFactor;
    public float jewelFactor;

    public float normalWaveTime;
    public float waveTime;
    public float timer;
    public bool spawning;

    // Start is called before the first frame update
    void Start()
    {
        //Normal ruleset values
        goldFactor = 1;
        coinFactor = 50;
        crownFactor = 1f;
        jewelFactor = 0.1f;

        normalWaveTime = 8;
        timer = GetComponent<RoundLogic>().pauseTime + Random.Range(1f, 2f);

        //Add each gold spawner to a list
        goldSpawners.AddRange(GetComponents<GoldSpawner>());
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        if(timer <= 0 && spawning)
        {
            SpawnGold();
        }
    }

    //Sets a common factor for all gold spawning in a round
    public void SetRound(float gold)
    {
        foreach (GoldSpawner gs in goldSpawners)
        {
            switch (gs.id)
            {
                case "coin":
                    gs.factor = coinFactor * gold;
                    break;
                case "crown":
                    gs.factor = crownFactor * gold;
                    break;
                case "jewel":
                    gs.factor = jewelFactor * gold;
                    break;
            }
        }

        waveTime = normalWaveTime;
        timer = waveTime;
    }

    //Individually set factor for each valuable
    public void SetRound(float coinf, float crownf, float jewelf, float wavet)
    {
        foreach (GoldSpawner gs in goldSpawners)
        {
            switch (gs.id)
            {
                case "coin":
                    gs.factor = coinf;
                    break;
                case "crown":
                    gs.factor = crownf;
                    break;
                case "jewel":
                    gs.factor = jewelf;
                    break;
            }
        }

        waveTime = wavet;
        timer = waveTime;
    }

    //Calls on every GoldSpawner to spawn a wave of gold
    public void SpawnGold()
    {
        foreach (GoldSpawner gs in goldSpawners)
        {
            gs.SpawnWave();
            Debug.Log("A wave of gold spawned");
        }

        timer = waveTime + Random.Range(-1, 1);

        //Shake the camera when a wave spawns
        CameraShake.Shake(Random.Range(0.2f, 0.4f), Random.Range(0.5f, 1f));
    }
}
