﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public List<GameObject> spawnPoints = new List<GameObject>();
    public List<GameObject> items = new List<GameObject>();
    public float spawnTime = 5f;

    public GameObject[] item;

    //Rock
    public GameObject[] rock;
    //rock end
    public GameObject bomb;
    public GameObject flame;
    public GameObject gas;
    public GameObject spike;

    //buffs
    public GameObject speedBuff;
    public GameObject shieldBuff;

    public List<Transform> emptySpot = new List<Transform> ();

    public GameObject stageObject;
    public Animator stageAnimator;

    public bool spawning;
    public float minAdd;
    public float maxAdd;

    public float spawnHeight;
    public float shakeTime;

    //factors
    public float hazardFactor;
    public float bombFactor;
    public float rockFactor;
    public float flameFactor;
    public float gasFactor;
    public float spikeFactor;
    public float speedBuffFactor;
    public float shieldBuffFactor;

    //timers
    public float hazardTimer;
    public float bombTimer;
    public float rockTimer;
    public float flameTimer;
    public float gasTimer;
    public float spikeTimer;
    public float speedBuffTimer;
    public float shieldBuffTimer;

    // Start is called before the first frame update
    void Start()
    {
        spawnPoints.AddRange(GameObject.FindGameObjectsWithTag("Spawner"));

        for (int i = 0; i < spawnPoints.Count; i++)
        {
            emptySpot.Add (spawnPoints[i].transform);
        }

        minAdd = -1;
        maxAdd = 1f;
        shakeTime = 3f;

        hazardFactor = 20f;

        spawnHeight = 30f;

        RandomizeStartup();
    }

    private void Update()
    {
        //Timers counting down
        hazardTimer -= Time.deltaTime;
        bombTimer -= Time.deltaTime;
        rockTimer -= Time.deltaTime;
        flameTimer -= Time.deltaTime;
        gasTimer -= Time.deltaTime;
        spikeTimer -= Time.deltaTime;
        speedBuffTimer -= Time.deltaTime;
        shieldBuffTimer -= Time.deltaTime;

        //Spawn stuff
        if (spawning) {
            if (bombTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnBomb();
            }
            if (rockTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnRock();
            }
            if (flameTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnFlame();
            }
            if (gasTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnGas();
            }
            if (spikeTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnSpike();
            }
            if (speedBuffTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnSpeedBuff();
            }
            if (shieldBuffTimer <= 0 && emptySpot.Count > 0)
            {
                SpawnShieldBuff();
            }
        }
    }

    void Awake()
    {
        //stageAnimator = stageObject.GetComponent<Animator>();
    }

    public void SetFactors(float f)
    {
        bombFactor = f;
        rockFactor = f;
        flameFactor = f;
        gasFactor = f;
        spikeFactor = f;
        shieldBuffFactor = f;
        speedBuffFactor = f;
}

    public void RandomizeStartup()
    {
        bombTimer = Random.Range(1, bombFactor);
        rockTimer = Random.Range(1, rockFactor);
        flameTimer = Random.Range(1, flameFactor);
        gasTimer = Random.Range(1, gasFactor);
        spikeTimer = Random.Range(1, spikeFactor);
        speedBuffTimer = Random.Range(1, speedBuffFactor);
        shieldBuffTimer = Random.Range(1, shieldBuffFactor);
    }

    void SpawnItem(GameObject hazard)
    {
        int spawnIndex = Random.Range(0, emptySpot.Count);

        GameObject NewItem = Instantiate(hazard, emptySpot[spawnIndex].transform.position, Random.rotation);
        NewItem.transform.Translate(Vector3.up * spawnHeight, Space.World);
        //stageAnimator.SetBool("Trap", true);
        if (hazard == bomb)
        {
            NewItem.GetComponent<DestroyObject>().mySpawnPoint = emptySpot[spawnIndex];
            emptySpot.RemoveAt(spawnIndex);
        }
    }

    void ShakeFloor(Transform t)
    {
        t.parent.GetComponent<Animator>().SetBool("Shaking", true);
        
    }

    void SpawnRock()
    {
        int rockIndex = Random.Range(0, rock.Length);
        SpawnItem(rock[rockIndex]);

        rockTimer = rockFactor + Random.Range(minAdd, maxAdd);
    }

    void SpawnBomb()
    {
        SpawnItem(bomb);

        bombTimer = bombFactor + Random.Range(minAdd, maxAdd);
    }

    void SpawnFlame()
    {
        Debug.Log("Spawning Flame");
        int spawnIndex = Random.Range(0, emptySpot.Count);
        var p = emptySpot[spawnIndex];
        emptySpot.RemoveAt(spawnIndex);

        ShakeFloor(p);

        StartCoroutine(cSpawnFlame(p));
        
        flameTimer = flameFactor + Random.Range(minAdd, maxAdd);
    }

    public IEnumerator cSpawnFlame(Transform t)
    {

        yield return new WaitForSeconds(shakeTime);

        if (spawning)
        {
            var NewFlame = Instantiate<GameObject>(flame, t.position, t.rotation);
            NewFlame.transform.Translate(Vector3.up * -3f, Space.World);

            NewFlame.GetComponent<DestroyObject>().mySpawnPoint = t;
        }
        else
        {
            emptySpot.Add(t);
        }

    }

    void SpawnGas()
    {
        Debug.Log("Spawning Gas");
        int spawnIndex = Random.Range(0, emptySpot.Count);
        var p = emptySpot[spawnIndex];
        emptySpot.RemoveAt(spawnIndex);

        ShakeFloor(p);

        StartCoroutine(cSpawnGas(p));
        
        gasTimer = gasFactor + Random.Range(minAdd, maxAdd);
    }

    public IEnumerator cSpawnGas(Transform t)
    {
        yield return new WaitForSeconds(shakeTime);

        if (spawning)
        {
            var NewGas = Instantiate<GameObject>(gas, t.position, t.rotation);
            NewGas.transform.Translate(Vector3.up * -3f, Space.World);

            NewGas.GetComponent<DestroyObject>().mySpawnPoint = t;
        }
        else
        {
            emptySpot.Add(t);
        }
    }

    void SpawnSpike()
    {
        Debug.Log("Spawning Spike");

        int spawnIndex = Random.Range(0, emptySpot.Count);
        var p = emptySpot[spawnIndex];
        emptySpot.RemoveAt(spawnIndex);

        ShakeFloor(p);

        StartCoroutine(cSpawnSpikes(p));

        spikeTimer = spikeFactor + Random.Range(minAdd, maxAdd);
    }

    public IEnumerator cSpawnSpikes(Transform t)
    {
        yield return new WaitForSeconds(shakeTime);

        if (spawning)
        {
            var NewSpike = Instantiate<GameObject>(spike, t.position, t.rotation);
            NewSpike.transform.Translate(Vector3.up * -3f, Space.World);

            NewSpike.GetComponent<DestroyObject>().mySpawnPoint = t;
        }
        else
        {
            emptySpot.Add(t);
        }
    }

    void SpawnSpeedBuff()
    {
        int spawnIndex = Random.Range(0, emptySpot.Count);

        var b = Instantiate<GameObject>(speedBuff, emptySpot[spawnIndex].position, speedBuff.transform.rotation);
        b.GetComponent<DestroyObject>().mySpawnPoint = emptySpot[spawnIndex];
        emptySpot.RemoveAt(spawnIndex);

        speedBuffTimer = speedBuffFactor + Random.Range(minAdd, maxAdd);
    }

    void SpawnShieldBuff()
    {
        int spawnIndex = Random.Range(0, emptySpot.Count);

        var b = Instantiate<GameObject>(shieldBuff, emptySpot[spawnIndex].position, shieldBuff.transform.rotation);
        b.GetComponent<DestroyObject>().mySpawnPoint = emptySpot[spawnIndex];
        emptySpot.RemoveAt(spawnIndex);
        
        shieldBuffTimer = shieldBuffFactor + Random.Range(minAdd, maxAdd);
    }
}
