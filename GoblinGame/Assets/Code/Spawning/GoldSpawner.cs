﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GoldSpawner : MonoBehaviour
{
    public float amount;

    public float spawnTime;
    public float origSpawnTime;
    public float spawnRadius;
    public float dropHeight;
    public bool spawningWave;

    public float factor;
    public float minAdd;
    public float maxAdd;

    public GameObject item;
    public string id;

    // Start is called before the first frame update
    void Start()
    {
        origSpawnTime = spawnTime;
    }

    private void Update()
    {
        //Spawns a wave of a valuable
        if (spawningWave)
        {
            for (int i = 1; i < amount; i++)
            {
                SpawnItem();
            }
            spawningWave = false;
        }
    }

    //Sets how many valuables will spawn in a wave
    public void SpawnWave()
    {
        //Randomizes the waves
        amount = factor + Random.Range(minAdd, maxAdd);
        spawningWave = true;
    }

    void SpawnItem()
    {

        // Good stuff here
        Vector3 pos = Random.insideUnitCircle * spawnRadius;

        GameObject NewItem = Instantiate(item,
            new Vector3(pos.x, dropHeight, pos.y),
            new Quaternion());

        NewItem.transform.Translate(Vector3.up * dropHeight);
        NewItem.transform.rotation = Random.rotation;
        StartCoroutine(PlayerController.EnableGold(1, NewItem));

    }
}
