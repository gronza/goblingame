﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpawner : MonoBehaviour
{
    public GameObject spawner;
    public GameObject music;

    // Start is called before the first frame update
    void Start()
    {
        if(GameObject.Find("PlayerSpawner(Clone)") == null)
        {
            Instantiate<GameObject>(spawner);
        }
        if (GameObject.Find("music(Clone)") == null)
        {
            //Instantiate<GameObject>(music);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
