﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour
{

    public AudioClip menu;
    public AudioClip level1;

    public AudioSource source;

    public float volume = 0.5f;
    public bool fade = false;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        var index = (SceneManager.GetActiveScene().buildIndex);
        source.volume = volume;

        if (index == 0)
        {
            if (source.clip != menu)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = menu;
                    source.Play();
                    fade = false;
                }
            }

        }

        if (index == 1)
        {
            if (source.clip != level1)
            {
                fade = true;
                volume -= Time.deltaTime;

                if (volume < 0.1f)
                {
                    source.Stop();
                    source.clip = level1;
                    source.Play();
                    fade = false;
                }
            }

        }


        if (!fade && volume < 0.5f)
        {
            volume += Time.deltaTime;
        }

        //SceneManager.activeSceneChanged;
    }

    //indestructible yeah
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}