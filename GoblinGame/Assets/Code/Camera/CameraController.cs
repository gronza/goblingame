﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public List<GameObject> players = new List<GameObject>();
    public Vector3 position;
    public Vector3 origPos;
    public float posY;

    // Start is called before the first frame update
    public void Start()
    {
        posY = transform.position.y;
        origPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        CalculatePosition();
        Move();
    }

    public void Initialize()
    {
        players.Clear();
        StartCoroutine(cInitialize());
    }

    public IEnumerator cInitialize()
    {
        yield return new WaitForEndOfFrame();
        players.AddRange(GameObject.FindGameObjectsWithTag("Player"));
        
    }

    //Calculates where the camera should be
    void CalculatePosition()
    {
        position = origPos;

        foreach (GameObject g in players)
        {
            position.x += g.transform.position.x / 5;
            position.z += g.transform.position.z / 5;
        }
    }

    //Slowly moves camera towards calculated position
    void Move()
    {
        //calculate the speed of camera movement to make it smooth
        var speed = Vector3.Distance(position, transform.position) / 2;

        transform.position = Vector3.MoveTowards(transform.position, position, speed * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, posY, transform.position.z);
    }
}
