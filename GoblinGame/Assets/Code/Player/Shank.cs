﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shank : MonoBehaviour
{
    public bool isShanking;
    public bool isCarrying;

    public BoxCollider box;
    public TrailRenderer trail;
    public bool canHit;
    public float activeTime;
    public float endLag;
    public AudioManager audio;

    // Start is called before the first frame update
    void Start()
    {
        box.enabled = false;
        trail.emitting = false;
        canHit = true;
        activeTime = 0.2f;
        endLag = 0.2f;
        audio = AudioManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        bool? doFire =
            GameManager.Inputs.GetButton(GetComponentInParent<PlayerController>().controller, InputConfig.Action.Fire1);

        //start a shank 
        if (doFire.HasValue && doFire.Value
            && canHit && !GetComponentInParent<PlayerController>().stunned)
        {
            isShanking = true;
            canHit = false;

            //TEST DROP GOLD FUNCTION
            //GetComponent<PlayerController>().DropGold(shank);
        }
    }

    public void Stab()
    {
        box.enabled = true;
        StartCoroutine(EndStab());
    }

    public IEnumerator EndStab()
    {
        yield return new WaitForSeconds(activeTime);
        StartCoroutine(EndLag());
        box.enabled = false;
        
        
    }

    public IEnumerator EndLag()
    {
        yield return new WaitForSeconds(endLag);
        isShanking = false;
        canHit = true;
    }

    public void EnableTrail()
    {
        //veks presentaatioo varte
        //trail.emitting = true;
    }

    public void DisableTrail()
    {
        trail.emitting = false;
    }

    public void LeftFoot()
    {
        int i = Random.Range(1, 5);
        audio.PlaySound("FS_L" + i.ToString());
    }

    public void RightFoot()
    {
        int i = Random.Range(1, 5);
        audio.PlaySound("FS_R" + i.ToString());
    }
    
}
