﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour , IComparable<PlayerController>
{
    public float playerId;
    public string name;
    public CapsuleCollider capsule;

    //variables for different player movement speeds
    public float origSpeed;
    public float speed;
    public float carryingSpeed;
    public float slowAreaSpeed;
    public float buffedSpeed;
    public float shankSpeed;

    public float lerpSpeed;
    public Rigidbody rb;

    //co-op stuffs
    public string horizontal;
    public string vertical;

    public Material defaultMat;
    public Material goldMat;

    public Shank shankController;

    public float score;
    public float finalScore;
    public Text scoreText;

    public GameObject spawner;
    public GameObject chest;
    public GameObject spawn;
    public Quaternion origRot;

    public bool stunned;
    public bool spinning;
    public float stunTime;

    //buffs
    public float speedBuffTimer;
    public float speedBuffTime;
    public float shieldBuffTimer;
    public float shieldBuffTime;
    public float buffRate;
    public bool buffed;

    public bool invulnerable;
    public float getUpShield;

    public bool slowed;
    public Collider other;

    public GameObject gold;
    public GameObject crown;
    public GameObject coin;
    public GameObject jewel;
    public List<float> valuables;
    public float totalValue;
    public float maxValue;

    public float goldPickupTime;
    public float goldThrowForce;

    public InputConfig.ControllerType controller;

    public Animator anim;
    public Transform goldPoint;
    public Transform indicatorPoint;
    public GameObject indicator;

    public RoundLogic round;

    //audiomanager
    AudioManager audioManager;

    // Start is called before the first frame update
    void Start()
    {
        origSpeed = 15f;
        carryingSpeed = 0.7f;
        slowAreaSpeed = 0.6f;
        buffedSpeed = 1.5f;
        shankSpeed = 0.8f;

        valuables = new List<float>();
        maxValue = 750f;

        goldPickupTime = 0.5f;
        goldThrowForce = 15;

        rb = GetComponent<Rigidbody>();
        capsule = GetComponent<CapsuleCollider>();
        shankController = GetComponentInChildren<Shank>();
        spawner = GameObject.Find("PlayerSpawner");
        origRot = transform.rotation;

        speedBuffTime = 5;
        shieldBuffTime = 5;
        stunned = true;
        spinning = false;
        getUpShield = 0.3f;

        anim = GetComponentInChildren<Animator>();
        indicator.transform.position = indicatorPoint.position;
        Instantiate<GameObject>(indicator,
            indicatorPoint.position,
            indicatorPoint.rotation,
            indicatorPoint);

        audioManager = AudioManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        //manage buffs
        speedBuffTimer -= Time.deltaTime;
        shieldBuffTimer -= Time.deltaTime;

        //speed
        if(speedBuffTimer > 0)
        {
            buffed = true;
        }
        else if(buffed)
        {
            buffed = false;
        }

        //shield
        if (shieldBuffTimer > 0)
        {
            invulnerable = true;

        }
        else if(invulnerable && transform.GetChild(2).gameObject.activeSelf)
        {
            invulnerable = false;
            transform.GetChild(2).gameObject.SetActive(false);
        }

        SetSpeed();
        
        //check if slowed and gas is destroyed
        if(slowed && !other)
        {
            slowed = false;
        }

        if (!stunned && !round.roundPaused)
        {
            Move();
            anim.SetBool("isStunned", false);
        }
        else if(spinning)
        {
            //ite oot obsoliitti ☉▵☉凸
            anim.SetBool("isStunned", true);
        }

        if (scoreText != null)
        {
            scoreText.text = score.ToString();
        }

        //set hit animation
        if (shankController.isShanking)
        {
            anim.SetBool("isAttacking", true);
        }
        else
        {
            anim.SetBool("isAttacking", false);
        }

        //set chest animation
        if (chest)
        {
            var canim = chest.transform.GetChild(0).GetComponent<Animator>();

            if (totalValue > 0)
            {
                canim.SetBool("isOpen", true);
            }
            else
            {
                canim.SetBool("isOpen", false);
            }

        }

        //Reset button for playtesting
        if (Input.GetKeyDown(KeyCode.R))
        {
            //Application.LoadLevel("SelectCharacter");
            Application.Quit();
        }
    }

    private void Move()
    {
        // Luetaan käyttäjän syöte Unityn Input luokasta
        float horizontal =
            GameManager.Inputs.GetAxis(controller, InputConfig.Action.Horizontal);
        float vertical =
            GameManager.Inputs.GetAxis(controller, InputConfig.Action.Vertical);
        


        //Lerp the velocity to achieve startup and stopping slide
        rb.velocity = Vector3.Lerp(rb.velocity, new Vector3(horizontal, 0, vertical).normalized * speed, lerpSpeed * Time.deltaTime);
        //Debug.Log(rb.velocity);
        
        if(horizontal != 0 || vertical != 0)
        {
            anim.SetBool("isMoving", true);
        }
        else
        {
            anim.SetBool("isMoving", false);
        }

            if (horizontal != 0 || vertical != 0)
        {
        transform.forward = rb.velocity;
        }
    }

    //Player's speed is set to normal speed,
    //then multiplied by each affecting factor
    //to make the player move slower in certain conditions
    public void SetSpeed()
    {
        speed = origSpeed;

        if (totalValue > 0)
        {
            //Needs core for carrying amount and speed relation
            speed *= 1 - totalValue/maxValue/5;
        }

        if (slowed)
        {
            speed *= slowAreaSpeed;
        }

        if (shankController.isShanking)
        {
            speed *= shankSpeed;
        }

        if (buffed)
        {
            speed *= buffedSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        //Slowing area
        if (other.CompareTag("SlowArea"))
        {
            slowed = true;
        }


        //pick up gold
                if (other.gameObject.CompareTag("Gold") &&
                    other.gameObject.GetComponent<GoldController>().value+totalValue <= maxValue)
                {
                    var v = other.gameObject.GetComponent<GoldController>().value;

                    valuables.Add(v);
                    totalValue += v;
                    FillBag(totalValue);
                    Destroy(other.gameObject);
                    audioManager.PlaySound("Coinpup1");
                }

        //buffs
        //speed buff
        if (other.gameObject.CompareTag("Buff"))
        {
            speedBuffTimer = speedBuffTime;
            Debug.Log("Rapido!");
            audioManager.PlaySound("Speedpup");
            round.isp.emptySpot.Add(other.gameObject.GetComponent<DestroyObject>().mySpawnPoint);
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("InvulnerableBuff"))
        {
            shieldBuffTimer = shieldBuffTime;
            Debug.Log("God Mode");
            audioManager.PlaySound("Shieldpup");
            transform.GetChild(2).gameObject.SetActive(true);
            round.isp.emptySpot.Add(other.gameObject.GetComponent<DestroyObject>().mySpawnPoint);
            Destroy(other.gameObject);
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("SlowArea"))
        {
            slowed = true;
            this.other = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("SlowArea"))
        {
            slowed = false;
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Gold"))
        {
            Physics.IgnoreCollision(collision.collider, capsule);
        }

        //drop gold in spawn
        if (collision.gameObject == chest)
        {
            if (shankController != null && totalValue > 0)
            {
                score += totalValue;
                totalValue = 0;
                valuables.Clear();
                FillBag(totalValue);
                audioManager.PlaySound("ReturnSmall");
            }
        }

    }

    //called when player is killed, respawns player at the set spawn position
    public void Respawn()
    {
        transform.position = spawn.transform.position;
        transform.rotation = origRot;
        rb.velocity = Vector3.zero;
        anim.SetBool("isMoving", false);
        anim.SetBool("isAttacking", false);
        anim.SetBool("isStunned", false);

        speedBuffTimer = 0;
        shieldBuffTimer = 0;

        stunned = true;
        spinning = false;
        totalValue = 0;
        valuables.Clear();
        FillBag(totalValue);
    }

    //called when player is hit by a stunning attack
    //player drops the gold they're carrying
    public void Stun(GameObject attacker)
    {
        if (!invulnerable)
        {
            stunned = true;
            spinning = true;
            invulnerable = true;
            StartCoroutine(WakeUp(stunTime));
            audioManager.PlaySound("Ghit2");

            DropGold(attacker);
            

            //sets player velocity to opposing direction of attacker
            rb.velocity = Vector3.Normalize(transform.position - attacker.transform.position) * speed / 2;
        }
    }

    //wakes the player up from a stun
    public IEnumerator WakeUp(float t)
    {
        yield return new WaitForSeconds(t);
        if (spinning)
        {
            stunned = false;
        }
        spinning = false;
        StartCoroutine(SetInvulnerability(getUpShield));
    }

    public IEnumerator SetInvulnerability(float t)
    {
        yield return new WaitForSeconds(t);
        invulnerable = false;
    }

    //player drops gold upon being hit
    public void DropGold(GameObject attacker)
    {
        //Drops every valuable the player is carrying
        foreach (float f in valuables)
        {
            //check which value an object has and spawn an according object
            SetGold(f);

            //direction between dropping player and attacking player
            var v = Vector3.Normalize(attacker.transform.position - transform.position);
            var r = UnityEngine.Random.Range(-100, 101);
            v = Quaternion.Euler(r, r, r) * v;

            var g = Instantiate<GameObject>(gold,
                    transform.position,
                    UnityEngine.Random.rotation);

            /*
            //initially ignores collision between gold cubes and players
            Physics.IgnoreCollision(capsule, g.GetComponent<BoxCollider>());
            Physics.IgnoreCollision(attacker.transform.parent.GetComponent<CapsuleCollider>(),
                g.GetComponent<BoxCollider>());
              */

            g.GetComponent<GoldController>().pickable = false;
            g.GetComponent<BoxCollider>().enabled = false;
            StartCoroutine(EnableGold(goldPickupTime, g));


            g.GetComponent<Rigidbody>().AddForce(-v * goldThrowForce / 4 + Vector3.up * goldThrowForce * 1.5f, ForceMode.Impulse);
            //add gold rotation here
            r = UnityEngine.Random.Range(5, 10);
            g.GetComponent<Rigidbody>().AddTorque(r, r, 0, ForceMode.Impulse);

        }
        valuables.Clear();
        totalValue = 0;
    }

    //Sets the correct gold piece according to value f
    private void SetGold(float f)
    {
        if (f == 100)
        {
            gold = crown;
        }
        else if (f == 1)
        {
            gold = coin;
        }
        else if (f == 500)
        {
            gold = jewel;
        }
    }

    //stop ignoring collision between spawned gold and players
    public static IEnumerator EnableGold(float t, GameObject g)
    {
        yield return new WaitForSeconds(t);
        if (g != null)
        {
            g.GetComponent<GoldController>().pickable = true;
            g.GetComponent<BoxCollider>().enabled = true;
        }
    }

    //Controls valuables in bag that indicate carried amount
    public void FillBag(float f)
    {
        //reset all children
        for (int i = 0; i < goldPoint.childCount; i++)
        {
            goldPoint.GetChild(i).gameObject.SetActive(false);
        }

        //start adding children
        if(f >= 1)
        {
            goldPoint.GetChild(0).gameObject.SetActive(true);
        }
        if (f >= 100)
        {
            goldPoint.GetChild(1).gameObject.SetActive(true);
        }
        if (f >= 500)
        {
            goldPoint.GetChild(2).gameObject.SetActive(true);
        }
    }

    //Default comparer
    int IComparable<PlayerController>.CompareTo(PlayerController other)
    {
        return other.finalScore.CompareTo(this.finalScore);
    }
}
