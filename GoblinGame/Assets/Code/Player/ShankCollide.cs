﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShankCollide : MonoBehaviour
{

    public PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInParent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == transform.parent)
        {
            Physics.IgnoreCollision(GetComponent<BoxCollider>(), transform.parent.GetComponent<CapsuleCollider>());
        }

        //also check if attacking player is stunned
        //to avoid both players getting stunned
        else if(other.CompareTag("Player") && !player.stunned)
        {
            other.GetComponent<PlayerController>().Stun(gameObject);
        }
    }
}
