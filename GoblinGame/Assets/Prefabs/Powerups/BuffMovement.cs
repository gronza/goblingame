﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffMovement : MonoBehaviour
{

    public float floatSpeed;
    public float turnSpeed;
    public Vector3 origPos;
    public float amplitude;

    // Start is called before the first frame update
    void Start()
    {
        turnSpeed = 100;
        floatSpeed = 50;
        origPos = transform.position;
        amplitude = 0.3f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime, Space.Self);

        var tempPos = origPos;
        tempPos.y = origPos.y + Mathf.Sin(Time.fixedTime * Mathf.PI * amplitude) * amplitude;

        transform.position = tempPos;
    }
}
